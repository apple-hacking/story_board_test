//
//  ViewController.swift
//  sss
//
//  Created by slbtty on 2022-06-11.
//

import Cocoa

class ViewController: NSViewController {
    
    @objc func buttonTest() {
        print("Test button")
    }
    
    // ctrl + drag item you can get this
    @IBOutlet weak var abtn: NSButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        abtn.action = #selector(self.buttonTest)
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

